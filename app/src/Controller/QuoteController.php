<?php

namespace App\Controller;

use App\Application\DTO\QuoteDTO;
use App\Application\Handler\QuoteHandler;
use App\Application\Query\PaginationQuery;
use App\Application\Query\Quote\QuoteQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QuoteController extends BaseController
{
    private QuoteHandler $quoteHandler;

    public function __construct(QuoteHandler $quoteHandler)
    {
        $this->quoteHandler = $quoteHandler;
    }

    public function index(Request $request): JsonResponse
    {
        return $this->ok($this->quoteHandler->getAll(new PaginationQuery($request), new QuoteQuery($request)));
    }

    public function show(int $id, Request $request): JsonResponse
    {
        return $this->ok($this->quoteHandler->getById($id, (new QuoteQuery($request))->getWith()));
    }

    public function create(Request $request): JsonResponse
    {
        $quoteIds = $this->quoteHandler->create(QuoteDTO::fromRequestForCreate($request));
        $headers = [];

        foreach ($quoteIds as $id) {
            $headers['Location'][] = "/quote/$id";
        }

        return $this->created($headers);
    }

    public function updateOne(int $id, Request $request): JsonResponse
    {
        $quoteId = $this->quoteHandler->updateOne($id, QuoteDTO::fromRequestFroUpdateOne($request));

        return $this->noContent(['Location' => "quote/$quoteId"]);
    }

    public function updateMany(Request $request): JsonResponse
    {
        $quoteIds = $this->quoteHandler->updateMany(QuoteDTO::fromRequestForUpdateMany($request));
        $headers = [];

        foreach ($quoteIds as $id) {
            $headers['Location'][] = "quote/$id";
        }

        return $this->noContent($headers);
    }

    public function deleteOne(int $id): JsonResponse
    {
        $this->quoteHandler->deleteOne($id);

        return $this->noContent();
    }

    public function deleteMany(Request $request): JsonResponse
    {
        $this->quoteHandler->deleteMany(QuoteDTO::fromRequestForDeleteMany($request));

        return $this->noContent();
    }
}
