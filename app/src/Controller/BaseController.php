<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseController extends AbstractController
{
    public function ok(array $data): JsonResponse
    {
        return $this->json(['data' => $data]);
    }

    public function created(array $headers = []): JsonResponse
    {
        return $this->json(null,201, $headers);
    }

    public function noContent(array $headers = []): JsonResponse
    {
        return $this->json(null, 204, $headers);
    }
}