<?php

namespace App\Controller;

use App\Application\Handler\AuthorHandler;
use App\Application\Query\PaginationQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthorController extends BaseController
{
    private AuthorHandler $authorHandler;

    public function __construct(AuthorHandler $authorHandler)
    {
        $this->authorHandler = $authorHandler;
    }

    public function index(Request $request): JsonResponse
    {
        return $this->ok($this->authorHandler->getAll(new PaginationQuery($request)));
    }
}
