<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Quote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class QuoteFixture extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 50; $i++) {
            /** @var Author $author */
            $author = $this->getReference(Author::class . '_' . rand(0, AuthorFixture::COUNT_AUTHORS - 1));

            $manager->persist(
                new Quote(
                    $this->faker->sentence(10),
                    $this->faker->year(),
                    $author
                )
            );
        }
        $manager->flush();
    }
}
