<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AuthorFixture extends Fixture
{
    const COUNT_AUTHORS = 50;

    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $uniqueNames = $this->generateUniqueNames();

        for ($i = 0; $i < self::COUNT_AUTHORS; $i++) {
            $author = new Author($uniqueNames[$i]);

            $manager->persist($author);

            $this->setReference(Author::class . '_' . $i, $author);
        }

        $manager->flush();
    }

    /** @return string[] */
    private function generateUniqueNames(): array
    {
        $uniqueNames = [];
        $name = $this->faker->name();

        while (self::COUNT_AUTHORS !== count($uniqueNames)) {
            if (in_array($name, $uniqueNames)) {
                $name = $this->faker->name();

                continue;
            }

            $uniqueNames[] = $name;
        }

        return $uniqueNames;
    }
}
