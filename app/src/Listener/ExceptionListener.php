<?php


namespace App\Listener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    private LoggerInterface $log;

    public function __construct(LoggerInterface $logger)
    {
        $this->log = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse();

        if ($exception instanceof NotFoundHttpException || $exception instanceof BadRequestHttpException) {
            $response->setStatusCode($exception->getCode());
            $response->setContent(json_encode($exception->getMessage()));
        } else {
            $this->log->error(sprintf(
                'Fatal error: %s, line: %d, code: %d, file: %s, trace: %s',
                $exception->getMessage(), $exception->getLine(), $exception->getCode(), $exception->getFile(), $exception->getTraceAsString()
            ));
            $response->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}