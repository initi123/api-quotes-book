<?php


namespace App\Listener;


use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RequestListener
{
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ('POST' === $request->getMethod()) {
            if ('json' !== $request->getContentType()) {
                throw new BadRequestHttpException('Content type must be of type json.');
            }

            if (!$request->getContent()) {
                throw new BadRequestHttpException('The content cannot be empty.');
            }
        }
    }
}