<?php


namespace App\Application\Handler;


use App\Application\DTO\QuoteDTO;
use App\Application\Mapper\QuoteMapper;
use App\Application\Query\PaginationQuery;
use App\Application\Query\Quote\QuoteQuery;
use App\Application\Query\Quote\QuoteQueryWith;
use App\Entity\Quote;
use App\Repository\AuthorRepository;
use App\Repository\QuoteRepository;
use ErrorException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuoteHandler
{
    private QuoteRepository $quoteRepository;
    private AuthorRepository $authorRepository;
    private ValidatorInterface $validator;

    public function __construct(QuoteRepository $quoteRepository, AuthorRepository $authorRepository, ValidatorInterface $validator)
    {
        $this->quoteRepository = $quoteRepository;
        $this->authorRepository = $authorRepository;
        $this->validator = $validator;
    }

    public function getAll(PaginationQuery $paginationQuery, QuoteQuery $quoteQuery): array
    {
        $quotes = $this->quoteRepository->getQuotesListByQuery($paginationQuery, $quoteQuery->getFilter());

        if (!$quotes) {
            throw new NotFoundHttpException(
                sprintf('Quotes doesn\'t exists by query params: %s', $quoteQuery->toString())
            );
        }

        return QuoteMapper::mapMany($quotes, $quoteQuery->getWith());
    }

    public function getById(int $id, QuoteQueryWith $quoteQueryWith): array
    {
        $quote = $this->quoteRepository->find($id);

        if (!$quote) {
            throw new NotFoundHttpException(
                sprintf('Quote by id %d doesn\'t exist', $id),
            );
        }

        return QuoteMapper::mapOne($quote, $quoteQueryWith);
    }

    /**
     * @param QuoteDTO[] $quotesDTO
     * @return int[]
     * @throws ErrorException
     */
    public function create(array $quotesDTO): array
    {
        $quotes = [];

        foreach ($quotesDTO as $quoteDTO) {
            $author = $this->authorRepository->find($quoteDTO->authorId);
            $quote = new Quote($quoteDTO->quote, $quoteDTO->year, $author);

            $this->validateQuote($quote);

            $quotes[] = $quote;
        }

        $quoteIds = $this->quoteRepository->saveMany($quotes);

        if (!$quoteIds) {
            throw new ErrorException('Error creating a quotes');
        }

        return $quoteIds;
    }

    public function updateOne(int $id, QuoteDTO $quoteDTO): int
    {
        $quote = $this->updateQuoteFields($id, $quoteDTO);
        $quoteId = $this->quoteRepository->saveOne($quote);

        if (!$quoteId) {
            throw new ErrorException('Error updating a quote: ' . $id);
        }

        return $quoteId;
    }

    /**
     * @param QuoteDTO[] $quotesDTO
     * @return int[]
     */
    public function updateMany(array $quotesDTO): array
    {
        $quotes = [];
        $quoteDTOIds = [];

        foreach ($quotesDTO as $quoteDTO) {
            $quoteDTOIds[] = $quoteDTO->id;
            $quotes[] = $this->updateQuoteFields($quoteDTO->id, $quoteDTO);
        }

        $quoteIds = $this->quoteRepository->saveMany($quotes);

        if (!$quoteIds) {
            throw new ErrorException('Error updating a quotes: ' . implode(',', $quoteDTOIds));
        }

        return $quoteIds;
    }

    private function updateQuoteFields(int $id, QuoteDTO $quoteDTO): Quote
    {
        if (!$quote = $this->quoteRepository->find($id)) {
            throw new NotFoundHttpException('Quote not found by id ' . $quoteDTO->id);
        }

        if ($quoteDTO->authorId) {
            if (!$author = $this->authorRepository->find($quoteDTO->authorId)) {
                throw new NotFoundHttpException('Author not found by id ' . $quoteDTO->authorId);
            }

            $quote->setAuthor($author);
        }

        if ($quoteDTO->quote) {
            $quote->setQuote($quoteDTO->quote);
        }

        if ($quoteDTO->year) {
            $quote->setYear($quoteDTO->year);
        }

        $this->validateQuote($quote);

        return $quote;
    }

    public function deleteOne(int $id): void
    {
        if (!$quote = $this->quoteRepository->find($id)) {
            throw new NotFoundHttpException('Quote not found by id ' . $id);
        }

        $this->quoteRepository->deleteOne($quote);
    }

    /**
     * @param QuoteDTO[] $quotesDTO
     */
    public function deleteMany(array $quotesDTO): void
    {
        foreach ($quotesDTO as $quoteDTO) {
            $this->deleteOne($quoteDTO->id);
        }
    }

    private function validateQuote(Quote $quote): void
    {
        $errors = $this->validator->validate($quote);

        if (count($errors) > 0) {
            throw new BadRequestHttpException((string)$errors);
        }
    }
}