<?php


namespace App\Application\Handler;


use App\Application\Mapper\AuthorMapper;
use App\Application\Query\PaginationQuery;
use App\Repository\AuthorRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthorHandler
{
    private AuthorRepository $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function getAll(PaginationQuery $paginationQuery): array
    {
        $authors = $this->authorRepository->getAuthorsList($paginationQuery);

        if (!$authors) {
            throw new NotFoundHttpException(
               'Authors don\'t exist'
            );
        }

        return AuthorMapper::mapMany($authors);
    }
}
