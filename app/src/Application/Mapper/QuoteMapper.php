<?php


namespace App\Application\Mapper;

use App\Application\Query\Quote\QuoteQueryWith;
use App\Entity\Quote;

class QuoteMapper
{
    /**
     * @param Quote[] $quotes
     */
    public static function mapMany(array $quotes, QuoteQueryWith $quoteQueryWith): array
    {
        $mappedQuotes = [];

        foreach ($quotes as $quote) {
            $mappedQuotes[] = self::mapOne($quote, $quoteQueryWith);
        }

        return $mappedQuotes;
    }

    public static function mapOne(Quote $quote, QuoteQueryWith $quoteQueryWith): array
    {
        $mappedQuote = [
            'id' => $quote->getId(),
            'year' => $quote->getYear(),
            'quote' => $quote->getQuote()
        ];

        return self::mapByWith($mappedQuote, $quote, $quoteQueryWith);
    }

    private static function mapByWith(array $mappedQuote, Quote $quote, QuoteQueryWith $quoteQueryWith): array
    {
        if ($quoteQueryWith->valueExist($quoteQueryWith::AUTHOR_WITH_KEY)) {
            $mappedQuote[$quoteQueryWith::AUTHOR_WITH_KEY] = [
                'id' => $quote->getAuthor()->getId(),
                'name' => $quote->getAuthor()->getName()
            ];
        }

        return $mappedQuote;
    }
}