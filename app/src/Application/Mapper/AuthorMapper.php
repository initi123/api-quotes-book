<?php


namespace App\Application\Mapper;


use App\Entity\Author;

class AuthorMapper
{
    /**
     * @param Author[] $authors
     */
    public static function mapMany(array $authors): array
    {
        $mappedAuthors = [];

        foreach ($authors as $author) {
            $mappedAuthors[] = [
                'id' => $author->getId(),
                'name' => $author->getName()
            ];
        }

        return $mappedAuthors;
    }
}