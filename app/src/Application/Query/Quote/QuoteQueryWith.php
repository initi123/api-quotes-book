<?php


namespace App\Application\Query\Quote;


use App\Application\Query\QueryInterface;
use Symfony\Component\HttpFoundation\Request;

class QuoteQueryWith implements QueryInterface
{

    const QUERY_WITH_KEY = 'with';
    const AUTHOR_WITH_KEY = 'author';

    private array $entities;

    public function __construct(Request $request)
    {
        $this->entities = $request->get(self::QUERY_WITH_KEY, []);
    }

    public function toString(): string
    {
        $string = '';

        foreach ($this->entities as $entity) {
            $string .= $entity;

            if ($entity !== end($this->entities)) {
                $string .=  ', ';
            }
        }

        return $string;
    }

    public function exist(): bool
    {
        return [] !== $this->entities;
    }

    public function valueExist(string $value): bool
    {
        return in_array($value, $this->entities);
    }
}