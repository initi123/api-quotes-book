<?php


namespace App\Application\Query\Quote;


use App\Application\Query\QueryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class QuoteQueryFilter implements QueryInterface
{

    const QUERY_FILTER_KEY = 'filter';

    const COUNT_VALUE_IN_FILTER = 1;

    const FILTER_ID_KEY = 'id';
    const FILTER_AUTHOR_IDS_KEY = 'author';
    const FILTER_YEAR_KEY = 'year';

    const AVAILABLE_VALUES = [
        self::FILTER_ID_KEY,
        self::FILTER_AUTHOR_IDS_KEY,
        self::FILTER_YEAR_KEY,
    ];

    private array $filters;

    public function __construct(Request $request)
    {
        $this->filters = $request->get(self::QUERY_FILTER_KEY, []);

        $this->checkFilters();
    }

    public function toString(): string
    {
        $string = '';

        foreach ($this->filters as $filter) {
            $key = key($filter);
            $string .= $key . ': ' . $filter[$key];

            if ($filter !== end($this->filters)) {
                $string .=  ', ';
            }
        }

        return $string;
    }

    public function exist(): bool
    {
        return [] !== $this->filters;
    }

    public function getIdsByFilters(): array
    {
        return self::convertIdsToInt(array_column($this->filters, self::FILTER_ID_KEY) ?? []);
    }

    public function getAuthorIdsByFilters(): array
    {
        return self::convertIdsToInt(array_column($this->filters, self::FILTER_AUTHOR_IDS_KEY) ?? []);
    }

    public function getYearsByFilters(): array
    {
        return array_column($this->filters, self::FILTER_YEAR_KEY) ?? [];
    }

    private function checkFilters(): void
    {
        foreach ($this->filters as $filter) {
            if (count($filter) > self::COUNT_VALUE_IN_FILTER) {
                throw new BadRequestHttpException('There can\'t be more than one value in one filter');
            }

            $name = key($filter) ?? '';

            if (!in_array($name, self::AVAILABLE_VALUES)) {
                throw new BadRequestHttpException('Unknown filter value: ' . $name);
            }
        }
    }

    private static function convertIdsToInt(array $ids): array
    {
        if (!$ids) {
            return [];
        }

        $convertedIds = [];

        foreach ($ids as $id) {
            $convertedIds[] = (int)$id;
        }

        return $convertedIds;
    }
}