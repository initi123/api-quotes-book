<?php


namespace App\Application\Query\Quote;


use App\Application\Query\QueryInterface;
use Symfony\Component\HttpFoundation\Request;

class QuoteQuery implements QueryInterface
{
    private QuoteQueryWith $with;
    private QuoteQueryFilter $filter;

    public function __construct(Request $request)
    {
        $this->with = new QuoteQueryWith($request);
        $this->filter = new QuoteQueryFilter($request);
    }

    public function toString(): string
    {
        $queryString = '';

        if ($this->filter->exist()) {
            $queryString .= 'filters: [' . $this->filter->toString() . '] ';
        }

        if ($this->with->exist()) {
            $queryString .= 'with: [' . $this->with->toString() . ']';
        }

        return $queryString;
    }

    public function getWith(): QuoteQueryWith
    {
        return $this->with;
    }

    public function setWith(QuoteQueryWith $with): self
    {
        $this->with = $with;

        return $this;
    }

    public function getFilter(): QuoteQueryFilter
    {
        return $this->filter;
    }

    public function setFilter(QuoteQueryFilter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }
}