<?php


namespace App\Application\Query;


use Symfony\Component\HttpFoundation\Request;

class PaginationQuery
{
    const DEFAULT_OFFSET = 0;
    const DEFAULT_LIMIT = 20;

    const OFFSET_KEY = 'offset';
    const LIMIT_KEY = 'limit';

    private int $offset;
    private int $limit;

    public function __construct(Request $request)
    {
        $this->offset = (int)$request->get(self::OFFSET_KEY, self::DEFAULT_OFFSET);
        $this->limit = (int)$request->get(self::LIMIT_KEY, self::DEFAULT_LIMIT);
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
}