<?php


namespace App\Application\Query;


use Symfony\Component\HttpFoundation\Request;

interface QueryInterface
{
    public function __construct(Request $request);

    public function toString(): string;
}