<?php


namespace App\Application\DTO;


use Symfony\Component\HttpFoundation\Request;

class QuoteDTO
{
    public int $id;

    public string $quote;

    public string $year;

    public int $authorId;

    /**
     * @return QuoteDTO[]
     */
    public static function fromRequestForCreate(Request $request): array
    {
        $requestData = json_decode($request->getContent(), true);
        $quotesDTO = [];

        foreach ($requestData as $quote) {
            $quotesDTO[] = self::buildDTO($quote, new self());
        }

        return $quotesDTO;
    }

    public static function fromRequestFroUpdateOne(Request $request): self
    {
        $quote = json_decode($request->getContent(), true);

        return self::buildDTO($quote, new self());
    }

    /**
     * @return QuoteDTO[]
     */
    public static function fromRequestForUpdateMany(Request $request): array
    {
        $requestData = json_decode($request->getContent(), true);
        $quotesDTO = [];

        foreach ($requestData as $quote) {
            $DTO = new self();
            $DTO->id = $quote['id'];

            $quotesDTO[] = self::buildDTO($quote, $DTO);
        }

        return $quotesDTO;
    }

    /**
     * @return QuoteDTO[]
     */
    public static function fromRequestForDeleteMany(Request $request): array
    {
        $requestData = json_decode($request->getContent(), true);
        $quotesDTO = [];

        foreach ($requestData as $quote) {
            $DTO = new self();
            $DTO->id = $quote['id'];

            $quotesDTO[] = $DTO;
        }

        return $quotesDTO;
    }

    private static function buildDTO(array $quote, self $DTO): self
    {
        $DTO->quote = $quote['quote'] ?? '';
        $DTO->year = $quote['year'] ?? '';
        $DTO->authorId = $quote['author_id'] ?? '';

        return $DTO;
    }
}