<?php

namespace App\Repository;

use App\Application\Query\PaginationQuery;
use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function getAuthorsList(PaginationQuery $paginationQuery)
    {
        $queryBuilder = $this->createQueryBuilder('author');

        if ($paginationQuery->getLimit()) {
            $queryBuilder->setMaxResults($paginationQuery->getLimit());
        }

        if ($paginationQuery->getOffset()) {
            $queryBuilder->setFirstResult($paginationQuery->getOffset());
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
