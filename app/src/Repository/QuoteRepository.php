<?php

namespace App\Repository;

use App\Application\Query\Quote\QuoteQueryFilter;
use App\Entity\Quote;
use App\Application\Query\PaginationQuery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Quote find($id, $lockMode = null, $lockVersion = null)
 */
class QuoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quote::class);
    }

    public function getQuotesListByQuery(PaginationQuery $paginationQuery, QuoteQueryFilter $quoteQueryFilter): array
    {
        $queryBuilder = $this->createQueryBuilder('quote');

        if ($paginationQuery->getLimit()) {
            $queryBuilder->setMaxResults($paginationQuery->getLimit());
        }

        if ($paginationQuery->getOffset()) {
            $queryBuilder->setFirstResult($paginationQuery->getOffset());
        }

        if ($quoteQueryFilter->exist()) {
            if ($filterByIds = $quoteQueryFilter->getIdsByFilters()) {
                $queryBuilder
                    ->orWhere($queryBuilder->expr()->in('quote.id', $filterByIds));
            }

            if ($filterByAuthors = $quoteQueryFilter->getAuthorIdsByFilters()) {
                $queryBuilder
                    ->orWhere($queryBuilder->expr()->in('quote.author', $filterByAuthors));
            }

            if ($filterByYear = $quoteQueryFilter->getYearsByFilters()) {
                $queryBuilder
                    ->orWhere($queryBuilder->expr()->in('quote.year', $filterByYear));
            }
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function saveOne(Quote $quote): ?int
    {
        $this->_em->persist($quote);
        $this->_em->flush();

        return $quote->getId();
    }

    /**
     * @param Quote[] $quoteCollection
     * @return int[]
     */
    public function saveMany(array $quoteCollection): array
    {
        $ids = [];

        foreach ($quoteCollection as $quote) {
            $ids[] = $this->saveOne($quote);
        }

        return $ids;
    }

    public function deleteOne(Quote $quote): void
    {
        $this->_em->remove($quote);
        $this->_em->flush();
    }

    /**
     * @param Quote[] $quoteCollection
     */
    public function deleteMany(array $quoteCollection): void
    {
        foreach ($quoteCollection as $quote) {
            $this->deleteOne($quote);
        }
    }
}
