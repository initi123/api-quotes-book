<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\QuoteRepository;

/**
 * @ORM\Entity(repositoryClass=QuoteRepository::class)
 *
 * @ORM\Table(name="quote", indexes = {
 *  @ORM\Index(name="idx_author_id", columns={"author_id"})
 * })
 */
class Quote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="text", unique="true")
     */
    private string $quote;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private string $year;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Author $author;

    public function __construct(string $quote, string $year, Author $author)
    {
        $this->quote = $quote;
        $this->year = $year;
        $this->author = $author;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function setAuthor(Author $author): self
    {
        $this->author = $author;

        return $this;
    }
}
